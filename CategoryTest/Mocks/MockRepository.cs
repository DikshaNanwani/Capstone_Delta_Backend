﻿using CategoryServices.DataLayer;
using CategoryServices.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CategoryTest.Mocks
{
    public static class MockRepository
    {
        public static Mock<ICategoryService> GetCategoryService()
        {
            var catlist = new List<EcomCategory>
            {
                new EcomCategory
                {
                    CategoryId = 300,
                    CategoryName = "Dress"
                },
                new EcomCategory
                {
                    CategoryId = 301,
                    CategoryName = "Electronics"
                }
            };

            var mockrepo = new Mock<ICategoryService>();

            mockrepo.Setup(r => r.GetAllCategory()).Returns(catlist);

            mockrepo.Setup(r => r.AddCategory(It.IsAny<EcomCategory>().ToString())).Returns((EcomCategory category) =>
            {
                catlist.Add(category);
                return catlist.SingleOrDefault(x => x.CategoryId == category.CategoryId);
            });

            return mockrepo;
        }
    }
}

