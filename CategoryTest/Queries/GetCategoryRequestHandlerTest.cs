﻿using CategoryServices.DataLayer;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using CategoryServices.Models;
using CategoryServices.Handlers;
using CategoryServices.Queries;
using Shouldly;
using System.Linq;

namespace CategoryTest.Queries
{
    public class GetCategoryRequestHandlerTest
    {
        private readonly Mock<ICategoryService> _mockRepo;

        public GetCategoryRequestHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetCategoryService();
        }

        [Fact]
        public async Task GetCategoryListTest()
        {
            var handler = new GetAllCategoryHandler(_mockRepo.Object);

            var result = await handler.Handle(new GetAllCategoryQuery(), CancellationToken.None);

            result.ShouldBeOfType<List<EcomCategory>>();

            result.Count().ShouldBe(2);
        }
    }
}
