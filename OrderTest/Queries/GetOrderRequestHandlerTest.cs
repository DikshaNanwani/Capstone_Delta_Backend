﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using OrderServices.DataLayer;
using OrderServices.Handlers;
using OrderServices.Queries;
using Shouldly;
using OrderServices.Models;
using System.Linq;

namespace OrderTest.Queries
{
    public class GetOrderRequestHandlerTest
    {
        private readonly Mock<IOrderService> _mockRepo;

        public GetOrderRequestHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetOrderService();
        }

        [Fact]
        public async Task GetOrderListTest()
        {
            var handler = new GetAllOrdersHandlers(_mockRepo.Object);

            var result = await handler.Handle(new GetAllOrdersQuery(), CancellationToken.None);

            result.ShouldBeOfType<List<EcomOrders>>();

            result.Count().ShouldBe(2);
        }
    }
}

