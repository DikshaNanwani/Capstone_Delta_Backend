﻿using Castle.Core.Resource;
using Moq;
using OrderServices.DataLayer;
using OrderServices.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderTest.Mocks
{
    public static class MockRepository
    {
        public static Mock<IOrderService> GetOrderService()
        {
            var catlist = new List<EcomOrders>
            {
                new EcomOrders
                {
                    OrderId = 300,
                    ProductId = 450,
                    CustomerId=90,
                    OrderQuantity=89,
                     OrderPrice=900,
                     ShipmentAddress="Nagpur"
                },
                new EcomOrders
                {
                    OrderId = 450,
                    ProductId = 550,
                    CustomerId=100,
                    OrderQuantity=99,
                     OrderPrice=800,
                     ShipmentAddress="Bhilai"
                }
            };

            var mockrepo = new Mock<IOrderService>();

            mockrepo.Setup(r => r.GetAllOrders()).Returns(catlist);

            

            return mockrepo;
        }
    }
}

