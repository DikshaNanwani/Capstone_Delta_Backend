﻿using Moq;
using ProductServices.DataLayer;
using ProductServices.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductTest.Mocks
{
    public static class MockRepository
    {
        public static Mock<IProductService> GetProductService()
        {
            var productlist = new List<EcomProducts>
            {
                new EcomProducts
                {
                    ProductId = 300,
                    CategoryId = 450,
                    ProductName="earings",
                    ProductType="Accessories",
                     ProductPrice=500,
                     ProductDescription="--"
                },
                new EcomProducts
                {
                   ProductId = 500,
                    CategoryId = 650,
                    ProductName="Jwellery",
                    ProductType="Accessories",
                     ProductPrice=700,
                     ProductDescription="--"
                }
            };

            var mockrepo = new Mock<IProductService>();

            mockrepo.Setup(r => r.GetAllProducts()).Returns(productlist);



            return mockrepo;
        }
    }
}

