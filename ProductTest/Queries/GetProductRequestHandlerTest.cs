﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using ProductServices.DataLayer;
using ProductServices.Handlers;
using ProductServices.Queries;
using ProductServices.Models;
using Shouldly;
using System.Linq;

namespace ProductTest.Queries
{
    public class GetProductRequestHandlerTest
    {
        private readonly Mock<IProductService> _mockRepo;

        public GetProductRequestHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetProductService();
        }

        [Fact]
        public async Task GetOrderListTest()
        {
            var handler = new GetAllProductsHandler(_mockRepo.Object);

            var result = await handler.Handle(new GetAllProductsQuery(), CancellationToken.None);

            result.ShouldBeOfType<List<EcomProducts>>();

            result.Count().ShouldBe(2);
        }
    }
}

 