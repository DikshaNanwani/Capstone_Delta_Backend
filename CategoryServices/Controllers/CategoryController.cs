﻿using CategoryServices.Command;
using CategoryServices.DataLayer;
using CategoryServices.Logger;
using CategoryServices.Models;
using CategoryServices.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CategoryServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {

        private IMediator mediator;
        private ILoggerService _loggerService;
        public CategoryController(IMediator mediator, ILoggerService loggerService)
        {
            this.mediator = mediator;
            _loggerService = loggerService;
        }


        [HttpGet("GetAllCategory")]
        public async Task<List<EcomCategory>> GetAllCategory()
        {
            _loggerService.LogInfo("Fetching All Categories list");
            return await mediator.Send(new GetAllCategoryQuery());
        }


        [HttpGet("GetCategoryById/{id}")]
        public async Task<EcomCategory> GetCategoryById(int id)
        {
            _loggerService.LogInfo("Fetching Category using CategoryId");
            return await mediator.Send(new GetCategoryByIdQuery() {categoryId=id });
        }

        [HttpPost("AddCategory/{category}")]
        public async Task<EcomCategory> AddCategory(string category)
        {
            _loggerService.LogInfo($"Adding Category into the DataBase Categoryname:{category}");
            return await mediator.Send(new AddCategoryCommand { categoryName=category });
        }

    }
}
