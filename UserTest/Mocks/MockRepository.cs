﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using UserServices.DataLayer;
using UserServices.Models;

namespace UserTest.Mocks
{
    public static class MockRepository
    {
        public static Mock<IUserService> GetUserService()
        {
            var customerlist = new List<EcomCustomers>
            {
                new EcomCustomers
                {
                    CustomerId = 300,
                    CustomerName = "Shanaya",
                    CustomerAddress="Banagalore",
                    CustomerPhoneNumber="9876543210",
                    CustomerEmailId="shanaya@gamilo.com",
                     LoginId =12
                },
                new EcomCustomers
                {
                     CustomerId = 600,
                    CustomerName = "Tejas",
                    CustomerAddress="Banagalore",
                    CustomerPhoneNumber="98556543210",
                    CustomerEmailId="tejas@gamilo.com",
                     LoginId =17

                }
            };

            var mockrepo = new Mock<IUserService>();

            mockrepo.Setup(r => r.GetCustomerById(It.IsAny<EcomCustomers>())).Returns((EcomCustomers customer) =>
            {
                
                return customerlist.SingleOrDefault(x => x.CustomerId == customer.CustomerId);
            });



            return mockrepo;
        }
    }
}
}
